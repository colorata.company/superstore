import java.io.File
import kotlin.reflect.KProperty
import androidx.compose.runtime.*

object SuperStorePreferences {
    var storageDir: String = ""
}

private object Recompose {
    fun recompose() {
        update = !update
    }

    @Composable
    fun OnPreferencesChanged(onRecompose: () -> Unit) {
        LaunchedEffect(key1 = update) {
            onRecompose()
        }
    }

    var update by mutableStateOf(false)
}

fun recompose() = Recompose.recompose()

@Composable
fun OnPreferencesChanged(onRecompose: () -> Unit) = Recompose.OnPreferencesChanged(onRecompose)

fun <T> mutablePreferenceOf(value: T, id: String, autoRecompose: Boolean = true) =
    SnapshotMutablePreference(value, id, autoRecompose = autoRecompose)

interface Preference<out T> {
    val value: T
}

operator fun <T> Preference<T>.getValue(thisObj: Any?, property: KProperty<*>): T = value

interface MutablePreference<T> : Preference<T> {
    override var value: T
    operator fun component1(): T
    operator fun component2(): (T) -> Unit
}

operator fun <T> MutablePreference<T>.setValue(thisObj: Any?, property: KProperty<*>, value: T) {
    this.value = value
}

@Suppress("UNCHECKED_CAST")
open class SnapshotMutablePreference<T>(
    value: T,
    val id: String,
    val path: String = "${SuperStorePreferences.storageDir}/preferences.txt",
    var autoRecompose: Boolean = true
) : MutablePreference<T> {
    override var value: T = value
        get() {
            val file = File(path)
            if (!file.exists()) file.createNewFile()
            val list = file.readLines().toMutableList()

            val finder = list.find {
                it.substring(0, it.indexOf(" -> ")) == id
            }
            println(finder)
            return if (finder == null) {
                file.appendText(
                    "$id -> $field\n"
                )
                field
            } else {
                val index = list.indexOf(finder)
                val substring = list[index].substring(id.length + 4)
                when (field) {
                    is Int -> substring.toInt() as T
                    is Boolean -> substring.toBoolean() as T
                    is Float -> substring.toFloat() as T
                    is String -> substring as T
                    else -> substring as T
                }
            }
        }
        set(value) {
            field = value
            if (autoRecompose) recompose()
            val file = File(path)
            if (!file.exists()) file.createNewFile()
            val list = file.readLines().toMutableList()
            val finder = list.find {
                it.substring(0, it.indexOf(" -> ")) == id
            }
            if (finder == null) {
                file.appendText(
                    "$id -> $value\n"
                )
            } else {
                val index = list.indexOf(finder)
                list[index] = "$id -> $value"
                file.writeText("")
                for (i in list) {
                    file.appendText("$i\n")
                }
            }
        }


    override fun component1(): T = value

    override fun component2(): (T) -> Unit = { value = it }
}
