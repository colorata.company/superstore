package com.colorata.superstore

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import java.io.File
import kotlin.reflect.KProperty

object SuperStorePreferences {
    /**
     * Init this value at the top level of your function (for example, OnCreate), this is your path to preferences file
     * Example:
     * ```java
     * SuperStorePreferences.storageDir =
     *      "/data/data/com.your.app/files/preferences.txt"
     * ```
     */
    var storageDir: String = ""
}

/**
 * You can create your *saveable* preference with this function.
 * Before using this function you must init ***SuperStorePreferences.storageDir***.
 * For more information, see [Guidelines](https://gitlab.com/colorata/superstore/-/blob/main/README.md)
 * @param value initial value of preference
 * @param id unique id of preference
 * @param getter if you want, you can implement custom getter for your class
 * @param setter if you want, you can implement custom setter for your class
 * @sample Preferences
 * @see SuperStorePreferences.storageDir
 */
@Suppress("UNCHECKED_CAST")
fun <T> mutablePreferenceOf(
    value: T,
    id: String,
    getter: (String) -> T? = { it as T? },
    setter: (T?) -> String = { it.toString() }
) = SnapshotMutablePreference(value, id, getter = getter, setter = setter)

data class Person(
    val name: String, val surname: String
)

object Preferences {
    var simplePreference by mutablePreferenceOf(0, "Preference")
    var customPreference by mutablePreferenceOf(
        Person("John", "Xina"),
        "User",
        getter = {
            val commaIndex = it.indexOf(',')
            require(commaIndex != -1) {
                "Invalid preference"
            }
            return@mutablePreferenceOf Person(
                it.substring(0 until commaIndex),
                it.substring(commaIndex + 1)
            )
        },
        setter = {
            return@mutablePreferenceOf "${it?.name}, ${it?.surname}"
        })
}

interface Preference<out T> {
    val value: T?
}

operator fun <T> Preference<T>.getValue(thisObj: Any?, property: KProperty<*>): T = value!!

interface MutablePreference<T> : Preference<T> {
    override var value: T?
    operator fun component1(): T
    operator fun component2(): (T) -> Unit
}

operator fun <T> MutablePreference<T>.setValue(thisObj: Any?, property: KProperty<*>, value: T) {
    this.value = value
}

@Suppress("UNCHECKED_CAST")
open class SnapshotMutablePreference<T>(
    private val classValue: T,
    private val id: String,
    private val getter: (String) -> T? = { it as T? },
    private val setter: (T?) -> String = { it.toString() }
) : MutablePreference<T> {

    private var _value: T? by mutableStateOf(null)
    override var value: T?
        get() {
            if (_value == null) {
                val file = File(SuperStorePreferences.storageDir)
                if (!file.exists()) file.createNewFile()
                val list = file.readLines().toMutableList()

                val finder = list.find {
                    it.substring(0, it.indexOf(" -> ")) == id
                }
                println(finder)
                return if (finder == null) {
                    file.appendText(
                        "$id -> $classValue\n"
                    )
                    _value = classValue
                    _value
                } else {
                    val index = list.indexOf(finder)
                    val substring = list[index].substring(id.length + 4)
                    _value = when (classValue) {
                        is Int -> substring.toInt() as T
                        is Boolean -> substring.toBoolean() as T
                        is Float -> substring.toFloat() as T
                        is String -> substring as T
                        else -> getter(substring)
                    }
                    when (classValue) {
                        is Int -> substring.toInt() as T
                        is Boolean -> substring.toBoolean() as T
                        is Float -> substring.toFloat() as T
                        is String -> substring as T
                        else -> getter(substring)
                    }
                }
            } else return _value
        }
        set(value) {
            _value = value
            val file = File(SuperStorePreferences.storageDir)
            if (!file.exists()) file.createNewFile()
            val list = file.readLines().toMutableList()
            val finder = list.find {
                it.substring(0, it.indexOf(" -> ")) == id
            }
            if (finder == null) {
                file.appendText(
                    "$id -> ${setter(value)}\n"
                )
            } else {
                val index = list.indexOf(finder)
                list[index] = "$id -> ${setter(value)}"
                file.writeText("")
                for (i in list) {
                    file.appendText("$i\n")
                }
            }
        }

    override fun component1(): T = value!!

    override fun component2(): (T) -> Unit = { value = it }

}