buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.android.tools.build:gradle:7.3.0-alpha03")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")

    }
}