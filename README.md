SuperStore is implementation of long-term preferences

# Import

SuperStore is available as library at *Jitpack.io*.

You can import it like other *Jitpack* libraries:

```java
// build.gradle(Project) -> allprojects -> repositories ->
maven { url "https://jitpack.io" }
```


```java
// build.gradle.kts(Project) -> allprojects -> repositories -> 
maven { url = uri("https://jitpack.io") }
```

Also, you need to put library dependency into app's *build.gradle*:
```java
// build.gradle(app) -> dependencies ->
implementation "com.gitlab.colorata:superstore:1.0.0"
```


```java
// build.gradle.kts(app) -> dependencies ->
implementation("com.gitlab.colorata:superstore:1.0.0")
```

# Usage

To specify preferences location, specify parent folder:

```java
// MainActivity -> onCreate ->
SuperStorePreferences.storageDir = filesDir.absolutePath
```

Now you can create preferences like this:

```java 
var preference by mutablePreferenceOf(10, "Preference")
```

First parameter(**10**) is default value. Second parameter(**"Preference"**) is unique id of parameter.

***Note:*** there are supported only few classes: **Int**, **String**, **Boolean**, **Float**. Support for other classes is planned.

# Guidelines
## Store Preferences
 ***✓ Do*** 
 Use all preferences in separated object:
```java
// (Object) Preferences ->
var preference by mutablePreferenceOf(10, "preference")
```

***! Caution***<br> Avoid using preferences like other values, it can create confusion between simple values and preferences:
```java
// Preferences.kt
var preference by mutablePreferenceOf(10, "preference")
```

## Composables
SuperStore supports seamless recompositions so you don't need to use *remember* or *mutableState*

***✓ Do*** <br>Use preferences in composables like other values:
```java 
@Composable
fun Compose() {
	Button(onClick = { Preferences.preference += 1 }) {
		Text(text = Preferences.preference.toString())
	}
}

// in this example, preferences will be updated automatically,
// without needing to put Preferences.preference into remember
// composition will recompose automatically when button clicked
```

***! Caution*** <br>
Avoid putting preferences into *remember* ***and*** *mutableState* braces, it can cause unexpected behaviors:
```java
@Composable
fun Compose() {
	var preference = remember { mutableState(Preferences.preference) }
}
```

## Content
***✓ Do*** <br> While using string as preference use all content in single line:
```java
// (Object) Preferences ->
var preference by mutablePreferenceOf("Value", "preference")
```

***✘ Don't*** <br>
Don't use `\n` or new lines in ***any*** fields , it will cause cutting preference values or unreachable preference. In this example, returned value will be `Value`, not `Value\nIS`:
```java
// (Object) Preferences ->
var preference by mutablePreferenceOf("preference", "Value\nIS")
```

## Custom classes
You can provide custom classes as saveables in SuperStore like this:
```java
data class Person(val name: String, val surname: String)
// (Object) Preferences -> 
var customPreference by mutablePreferenceOf(
        Person("John", "Xina"),
        "User",
        getter = {
            val commaIndex = it.indexOf(',')
            require(commaIndex != -1) {
                "Invalid preference"
            }
            return@mutablePreferenceOf Person(
                it.substring(0 until commaIndex),
                it.substring(commaIndex + 1)
            )
        },
        setter = {
            return@mutablePreferenceOf "${it?.name}, ${it?.surname}"
        })
```

# TroubleShooting
## Importing
If you are expecting issues with importing library, you can also add this to your *settings.gradle* file:
```java
// settings.gradle -> dependencyResolutionManagement -> repositories ->
maven {	url = "https://jitpack.io" }
```


```java
// settings.gradle.kts -> dependencyResolutionManagement -> repositories ->
maven { url = uri("https://jitpack.io") }
```
